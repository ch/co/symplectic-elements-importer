# this file is a perl fragment to be eval'ed, so we should be
# sure it's really valid perl!
use strict;
use warnings;

# allow us to use $LOGLEVEL constants if we want to set $main::loglevel
use FindBin::libs;
use Elements::Constants qw ( %LOGLEVEL );

$main::dbuser = 'databaseUser';
$main::dbpass = 'databasePassword';
$main::dbhost = '127.0.0.1';
$main::dbname = 'elements';
$main::dbpublicationstable = 'publications';
$main::elementsgroups = '';

# needs to be username:password
$main::apicreds = 'elementsAPIuser:elementsAPIpassword';

$main::apibaseurl = 'https://elements.admin.cam.ac.uk:8092/elements-api/v5.5';

# defaults to zero. The precise effect of non-zero values
# should be deduced by reading Elements/Constants.pm
$main::loglevel = 1;

1;

