#!/usr/bin/perl 

use strict;
use warnings;

use FindBin::libs;

use Elements::PublicationParser;
use Elements::UserParser;
use Elements::UserRelationParser;
use Elements::Database;
use Elements::Crawler;
use Elements::Config;
use Elements::Constants qw ( %LOGLEVEL );
use Data::Dumper;
use DBI;

my $config = Elements::Config->new('/etc/elements-importer/config.pl');

my $loglevel = $config->loglevel;

my $dbUser = 'elements_user';
my $dbPass = $config->dbpass;
my $dbHost = $config->dbhost;
my $dbName = $config->dbname;
my $api_base = $config->apibaseurl;

my $apicreds = $config->apicreds;

my $publicationsTable = $config->dbpublicationstable;
my $elementsGroups = $config->elementsgroups;

my $db = Elements::Database->new();
$db->dbUser($dbUser);
$db->dbPass($dbPass);
$db->dbHost($dbHost);
$db->dbName($dbName);
$db->publicationsTable($publicationsTable);
$db->connectToDB;

my $publicationParser = Elements::PublicationParser->new();
my $userParser = Elements::UserParser->new();
my $relationParser = Elements::UserRelationParser->new();

my $crawler=Elements::Crawler->new();
my $itemsPerPage;

### crawl publications ###

if($loglevel & $LOGLEVEL{CRAWLED_PUBLICATIONS}) {
  $crawler->verbose(1);
  $crawler->savePrefix('publications');
  $crawler->saveToFile(1);
}
$crawler->delay(2/60); # time unit is minutes here
$crawler->timeout(30); # seconds
$crawler->apicreds($apicreds);
$crawler->stopAfterPage(4000);

$itemsPerPage=25; # max of 25 publications per page when detail=full
$crawler->addUrlOption('groups', $elementsGroups);
$crawler->addUrlOption('per-page', $itemsPerPage);
$crawler->addUrlOption('detail', 'full');

$crawler->baseUrl($api_base);
$crawler->resourceType('publications');

do {
  my $response = $crawler->crawlOnePage;
  my %publications = $publicationParser->parsePublicationsFromString($response);

  for my $id (keys %publications) {
    $db->addPublication($id, $publications{$id});
    if(@{$publications{$id}->{'#duplicates'}}) {
      foreach my $duplicate (@{$publications{$id}->{'#duplicates'}}) {
        $db->deletePublication($duplicate);
      }
    }

  }
} while($crawler->keepGoing);

### crawl users ###

$crawler = Elements::Crawler->new();
if($loglevel & $LOGLEVEL{CRAWLED_USERS}) {
  $crawler->verbose(1);
  $crawler->savePrefix('users');
  $crawler->saveToFile(1);
}
$crawler->apicreds($apicreds);

$crawler->stopAfterPage(500);

$crawler->baseUrl($api_base);
$crawler->resourceType('users');

$crawler->delay(5/60); # time unit is minutes..
$crawler->timeout(60); # seconds

$itemsPerPage=200;
$crawler->addUrlOption('detail', 'ref');
$crawler->addUrlOption('groups', $elementsGroups);
$crawler->addUrlOption('per-page', $itemsPerPage);


my @users = ();
do {
  my $response = $crawler->crawlOnePage;
  my %users = $userParser->parseUsersFromString($response);
  for my $id (keys %users) {
    $db->addUser($id, $users{$id});
    push(@users, $id);
  }
} while ($crawler->keepGoing);

### crawl relationships (specifically authorships, type=8) for each user ###

foreach my $userid (@users) {
  $crawler = Elements::Crawler->new();
  if($loglevel & $LOGLEVEL{CRAWLED_RELATIONSHIPS}) {
    $crawler->verbose(1);
    $crawler->savePrefix("relationships-$userid");
    $crawler->saveToFile(1);
  }

  $crawler->baseUrl($api_base);
  $crawler->resourceType('relationships');
  $crawler->apicreds($apicreds);

  $crawler->delay(5/60); # time unit is minutes..
  $crawler->timeout(60); # seconds

  $crawler->addUrlOption('involving', "user($userid)");
  $crawler->addUrlOption('types', '8');
  $crawler->addUrlOption('per-page', '400');

  my %mm_author = ();
  my %mm_fav = ();

  do {
    my $response = $crawler->crawlOnePage;
    my ($authors, $fav) = $relationParser->parseUserRelationsFromString($response);
    push(@{$mm_author{$userid}}, @{$authors});
    push(@{$mm_fav{$userid}}, @{$fav});
  } while ($crawler->keepGoing);

  $db->add_mm_author(%mm_author);
  $db->add_mm_favourites(%mm_fav);
}
