package Elements::Config;

use strict;
use warnings;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {
    'filename' => shift,
    'dbpass' => undef,
    'dbhost' => undef,
    'dbname' => undef,
    'apicreds' => undef,
    'apibaseurl' => undef,
    'dbpublicationstable' => undef,
    'elementsgroups' => undef,
    'loglevel' => 0,
  };

  bless($self, $class);
  if ($self->filename) {
    $self->parseFile;
  }
  return $self;
}

sub filename {
  my $self = shift;
  return $self->{'filename'};
}

sub dbpublicationstable {
  my $self = shift;
  if (@_) { $self->{'dbpublicationstable'} = shift; }
  return $self->{'dbpublicationstable'};
}

sub dbpass {
  my $self = shift;
  if (@_) { $self->{'dbpass'} = shift; }
  return $self->{'dbpass'};
}

sub loglevel {
  my $self = shift;
  if (@_) { $self->{'loglevel'} = shift; }
  return $self->{'loglevel'};
}

sub dbhost {
  my $self = shift;
  if (@_) { $self->{'dbhost'} = shift; }
  return $self->{'dbhost'};
}

sub dbname {
  my $self = shift;
  if (@_) { $self->{'dbname'} = shift; }
  return $self->{'dbname'};
}

sub apicreds {
  my $self = shift;
  if (@_) { $self->{'apicreds'} = shift; }
  return $self->{'apicreds'};
}

sub apibaseurl {
  my $self = shift;
  if (@_) { $self->{'apibaseurl'} = shift; }
  return $self->{'apibaseurl'};
}

sub elementsgroups {
  my $self = shift;
  if (@_) { $self->{'elementsgroups'} = shift; }
  return $self->{'elementsgroups'};
}

sub parseFile {
  my $self = shift;
  require $self->filename;
  $self->apicreds($main::apicreds);
  $self->apibaseurl($main::apibaseurl);

  $self->dbpass($main::dbpass);

  $self->dbname($main::dbname);
  $self->dbhost($main::dbhost);

  $self->dbpublicationstable($main::dbpublicationstable);
  $self->elementsgroups($main::elementsgroups);
 
  $self->loglevel($main::loglevel) if defined $main::loglevel;

  undef(%main::apicreds);
  undef(%main::apibaseurl);
  undef(%main::dbpass);
  undef(%main::dbname);
  undef(%main::dbhost);
  undef(%main::dbpublicationstable);
  undef(%main::elementsgroups);
  undef(%main::loglevel);
}

1;
