package Elements::Constants;

use strict;
use warnings;

use base 'Exporter';
use Const::Fast;

const our %LOGLEVEL => (
  CRAWLED_PUBLICATIONS => 0x1,
  CRAWLED_USERS => 0x2,
  CRAWLED_RELATIONSHIPS => 0x4,
);

our @EXPORT_OK = qw ( %LOGLEVEL );

1;
