package Elements::Crawler;

use strict;
use HTTP::Status qw(:DEFAULT is_client_error);
use LWP::RobotUA;
use MIME::Base64;
use Encode qw(encode);
use URI;
use XML::LibXML;
use XML::LibXML::XPathContext;
use Data::Dumper;
use IO::Handle;
use File::Spec;

use URI::Escape;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {
    'baseUrl' => undef,
    'apicreds' => undef,
    'apicreds_header' => undef,
    'resourceType' => undef,
    'urlOptions' => {},
    'ua' => undef,
    'saveDir' => '/tmp',
    'savePrefix' => 'page',
    'currentPage' => 1,
    'stopAfterPage' => 100000,
    'keepGoing' => 0,
    'saveToFile' => 0,
    'verbose' => 0,
    'crawllogfile' => '/tmp/elementscrawl.log',
    'crawllogfh' => undef,
    'xc' => XML::LibXML::XPathContext->new(),
    'parser' => XML::LibXML->new()
  };
  $self->{'ua'} = LWP::RobotUA->new('Dept Of Chemistry Publication Downloader', 'support@ch.cam.ac.uk');

  bless($self, $class);
  if($self->verbose && $self->crawllogfile) {
    open(my $fh, ">>", $self->crawllogfile) or die "can't open " . $self->crawllogfile;
    $fh->autoflush;
    $self->crawllogfh($fh);
    $self->crawllogmark;
  }
  return $self;
}

sub getElementsFeedFromURL {
  my $self = shift;
  my $url = $self->url;
  my $response = $self->downloadURL;
  my $doc = $self->parser->parse_string($response);
# next need to set up XML parser and register namespaces
  $self->xc->setContextNode($doc->documentElement());
# default namespace used in the Elements feed
  $self->xc->registerNs ( atom => 'http://www.w3.org/2005/Atom' );
  return $response;
}

sub url {
  my $self = shift;
  die "resourceType not set" unless $self->resourceType;
  my $str = $self->baseUrl . "/" . $self->resourceType . "?";
  while(my($k,$v)=each(%{$self->urlOptions})) {
    $str .= "$k=$v&";
  }	
  chop $str;
  return $str;
}

sub resetCrawler {
  my $self = shift;
  $self->currentPage(1);
  $self->xc->setContextNode(undef);
}

sub apicreds {
  my $self = shift;
  if (@_) {
    $self->{'apicreds'} = shift;

    # https://support.symplectic.co.uk/support/solutions/articles/6000050010-api-user-guide
    # 1) Create a text string consisting of the username followed by a colon followed by the password e.g. "username:password".
    # 2) Encode this string to a byte array using UTF-8.
    # 3) Convert the byte array to its base 64 text encoding.
    # 4) Append the base 64 encoded credentials to the text "Basic " (note the trailing space).

    my $enc_creds = encode_base64($self->apicreds, '');
    $enc_creds=encode('UTF-8', $enc_creds);
    $self->apicreds_header("Basic $enc_creds");
  }
  return $self->{'apicreds'};
}

sub apicreds_header {
  my $self = shift;
  if (@_) { $self->{'apicreds_header'} = shift; }
  return $self->{'apicreds_header'};
}

sub currentPage {
  my $self = shift;
  if (@_) { $self->{'currentPage'} = shift; }
  return $self->{'currentPage'};
}

sub saveToFile {
  my $self = shift;
  if (@_) { $self->{'saveToFile'} = shift; }
  return $self->{'saveToFile'};
}

sub verbose {
  my $self = shift;
  if (@_) {
    $self->{'verbose'} = shift;
    if($self->verbose) {
       open(my $fh, ">>", $self->crawllogfile) or die "can't open logfile " . $self->crawllogfile;
       $fh->autoflush;
       $self->crawllogfh($fh);
       $self->crawllogmark;
    }
  }
  return $self->{'verbose'};
}

sub crawllogfile {
  my $self = shift;
  if (@_) { $self->{'crawllogfile'} = shift; }
  return $self->{'crawllogfile'};
}

sub crawllogfh {
  my $self = shift;
  if (@_) { $self->{'crawllogfh'} = shift; }
  return $self->{'crawllogfh'};
}

sub crawllogmark {
  my $self = shift;
  print { $self->crawllogfh } "Log file (re)opened at " . localtime() . "\n";
}

sub keepGoing {
  my $self = shift;
  if (@_) { $self->{'keepGoing'} = shift; }
  return $self->{'keepGoing'};
}

sub clearUrlOptions {
  my $self = shift;
  for(keys %{$self->urlOptions}) {  delete ${$self->urlOptions}{$_}; }
  $self->currentPage(1);
}

sub addUrlOption {
  my $self = shift;
  while (my $k=shift) {
    my $v = shift;
    ${$self->urlOptions}{$k}=uri_escape($v);
  }
}

sub getUrlOption {
  my $self = shift;
  while (my $k=shift) {
    return ${$self->urlOptions}{$k};
  }
}

sub crawl {
  my $self = shift;
  my $noMorePages = 0;
  my $response;

  until ($noMorePages || $self->currentPage > $self->stopAfterPage) {
    print { $self->crawllogfh } "downloading ".$self->url."\n" if $self->verbose;
    $response = $self->getElementsFeedFromURL;
    if($self->saveToFile) {
      my $filename = File::Spec->catfile($self->saveDir, $self->savePrefix."-".$self->currentPage);
      open OUTPUT, ">$filename" or die $!;
      print OUTPUT $response;
      close OUTPUT;
    }
    $noMorePages = 1 unless $self->setNextPage;
    $self->currentPage($self->currentPage + 1);
  }
  return $response;
}

sub crawlOnePage {
  my $self = shift;
  my $stopAfterPage = $self->stopAfterPage;
  my $response;
  print { $self->crawllogfh } "downloading ".$self->url."\n" if $self->verbose;
  $response = $self->getElementsFeedFromURL;
  if($self->saveToFile) {
    my $filename = File::Spec->catfile($self->saveDir, $self->savePrefix."-".$self->currentPage);
    open OUTPUT, ">$filename" or die $!;
    print OUTPUT $response;
    close OUTPUT;
  }

  $self->currentPage($self->currentPage + 1);
  if($self->currentPage > $self->stopAfterPage) {
    $self->keepGoing(0);
  }
  $self->setNextPage and $self->keepGoing(1) or $self->keepGoing(0);
  return $response;
}

sub downloadURL {
  my $self = shift;
  my $url = $self->url;
  my $response;
  my $attempts = 0;
  my $maxAttempts = 8;
  my $success = 0;
  my $initialDelayTime = $self->ua->delay;

  my @headers;
  if($self->apicreds_header) {
    push(@headers, 'Authorization' => $self->apicreds_header);
  }

  until($success || $attempts > $maxAttempts) {
    $response = $self->ua->get($url, @headers);
    if($response->is_success) { $success = 1; last; }
    if(HTTP::Status::is_client_error($response->code)) { die "problem with $url:\n".$response->status_line; }
    if($response->code eq 500) { print $response->status_line."\n"; }
    $attempts++;
    my $delay = $self->delay*2;
    $self->delay($delay);
    print { $self->crawllogfh } "delaying for ".sprintf("%.2f",$self->delay)." minutes\n" if $self->verbose;
  }
  unless($response->is_success) {
    die $response->status_line;
  }

  $self->delay($initialDelayTime);
  return $response->content;
}

sub setNextPage {
  my $self = shift;
  my $nextPageXpath = '/atom:feed/api:pagination/api:page[@position="next"]';
  my $after_id;
  if($self->xc->getContextNode) {
    my $nextpageNode = $self->getNodeFromXpath($nextPageXpath);
    if(defined($nextpageNode)) {
      my $next_href = URI->new($nextpageNode->getAttribute('href'));
      my %query = $next_href->query_form;

      $after_id = $query{'after-id'};

      $self->addUrlOption('after-id', $after_id);
      if($self->resourceType eq 'relationships') {
        my $modified_since = $query{'modified-since'};
        $self->addUrlOption('modified-since', $modified_since);
      } else {
        my $affected_since = $query{'affected-since'};
        $self->addUrlOption('affected-since', $affected_since);
      }

    } else {
      return 0;
    }
  } else {
    return 0;
  }
  return 1;
}

sub getNodeFromXpath {
  my $self = shift;
  my $xPath = shift;
  my $result;
  my $queryResult = $self->xc->findnodes($xPath);
  my $count = $queryResult->size();
  if($count == 1) {
    return $queryResult->get_node(1);
  } else {
    return;
  }
}

sub printRobotInfo {
  my $self=shift;
  my $ua = $self->ua;
  print Dumper($ua);
}

sub baseUrl {
  my $self = shift;
  if(@_) { $self->{'baseUrl'} = shift; }
  return $self->{'baseUrl'};
}

sub resourceType {
  my $self = shift;
  if(@_) {
    $self->{'resourceType'} = shift;
    my @validResources = ('publications', 'users', 'relationships');
    die "invalid resourceType" unless grep { $_ eq $self->{'resourceType'} } @validResources;
  }
  return $self->{'resourceType'};
}

sub urlOptions {
  my $self = shift;
  if(@_) { $self->{'urlOptions'} = shift; }
  return $self->{'urlOptions'};
}

sub saveDir {
  my $self = shift;
  if(@_) { 
    my $dir = shift;
    if(substr($dir, -1, 1) ne "/") { $dir.="/"; }
    $self->{'saveDir'} = $dir; 
  }
  return $self->{'saveDir'};
}

sub savePrefix {
  my $self = shift;
  if(@_) { $self->{'savePrefix'} = shift; }
  return $self->{'savePrefix'};
}

sub delay {
  my $self = shift;
  if(@_) { 
    $self->ua->delay(shift);
  }
  return $self->ua->delay;
}

sub timeout {
  my $self = shift;
  if(@_) { 
    $self->ua->timeout(shift); 
  }
  return $self->{'timeout'};
}

sub ua {
  my $self = shift;
  return $self->{'ua'};
}

sub xc {
  my $self = shift;
  return $self->{'xc'};
}

sub parser {
  my $self = shift;
  return $self->{'parser'};
}

sub stopAfterPage {
  my $self = shift;
  if(@_) { $self->{'stopAfterPage'} = shift; }
  return $self->{'stopAfterPage'};
}

1;
