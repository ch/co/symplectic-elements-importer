package Elements::Database;

my $debug = 0;

use strict;
use warnings;

use Data::Dumper;
use DBI;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {
    'dbUser' => undef,
    'dbPass' => undef,
    'dbHost' => undef,
    'dbName' => undef,
    'publicationsTable' => 'publications',
    'usersTable' => 'users',
    'mm_author_table' => 'mm_user_publications_author',
    'mm_favourites_table' => 'mm_user_publications_favourite',
    'dbh' => undef,		
  };
  bless($self, $class);
  return $self;
}

sub dbh {
  my $self = shift;
  return $self->{'dbh'};
}

sub connectToDB {
  my $self = shift;
  $self->{'dbh'} = DBI->connect($self->dataSource, $self->dbUser, $self->dbPass) or die;
}

sub dataSource {
  my $self = shift;
  return "dbi:Pg:dbname=".$self->dbName.";host=".$self->dbHost;
}

sub dbUser {
  my $self = shift;
  if (@_) { $self->{'dbUser'} = shift; }
  return $self->{'dbUser'};
}

sub dbPass {
  my $self = shift;
  if (@_) { $self->{'dbPass'} = shift; }
  return $self->{'dbPass'};
}

sub dbHost {
  my $self = shift;
  if (@_) { $self->{'dbHost'} = shift; }
  return $self->{'dbHost'};
}

sub dbName {
  my $self = shift;
  if (@_) { $self->{'dbName'} = shift; }
  return $self->{'dbName'};
}

sub publicationsTable {
  my $self = shift;
  if (@_) { $self->{'publicationsTable'} = shift; }
  return $self->{'publicationsTable'};
}

sub usersTable {
  my $self = shift;
  if (@_) { $self->{'usersTable'} = shift; }
  return $self->{'usersTable'};
}

sub addPublication {
  my $self = shift;
  my $id = shift;
  my $publication = shift;

  if ($self->publicationIdExistsInDB($id)) {
    $self->updateObjectInDB($id, $publication, $self->publicationsTable);
  } else {
    $self->addObjectToDB($id, $publication, $self->publicationsTable);
  }
}

sub deletePublication {
  my $self = shift;
  my $id = shift;
  $self->deleteObjectFromDB($id, $self->publicationsTable);
}

sub addUser {
  my $self = shift;
  my $id = shift;
  my $user = shift;

  if($self->userIdExistsInDB($id)) {
    $self->updateObjectInDB($id, $user, $self->usersTable);
  } else {
    $self->addObjectToDB($id, $user, $self->usersTable);
  }
}


sub publicationIdExistsInDB {
  my $self = shift;
  my $id = shift;
  return $self->idExistsInDBTable($id, $self->publicationsTable);
}

sub userIdExistsInDB {
  my $self = shift;
  my $id = shift;
  return $self->idExistsInDBTable($id, $self->usersTable);
}

sub idExistsInDBTable {
  my $self = shift;
  my $id = shift;
  my $table = shift;

  my $sql = "select 'id' from $table where id='$id'";
  my $numrows = $self->dbh->do($sql);
  if($numrows == 1) { return 1; }
}

sub addObjectToDB {
  my $self = shift;
  my $id = shift;
  my $objectRef = shift;
  my $table = shift;
  my %object = %$objectRef;
  my $fieldStr="(id, ";
  my $valStr="(\$\$$id\$\$, ";
  foreach my $k (keys %object) {
    next if substr($k,0,1) eq '#';
    if($object{$k}) {
      $valStr .= $self->dbh->quote($object{$k}) . ", ";
      $fieldStr .= $k.", ";
    }
  }

  chop $valStr; chop $valStr;
  chop $fieldStr; chop $fieldStr;
  $valStr.=")";
  $fieldStr.=")";
  my $sql = "insert into ".$table." $fieldStr values $valStr";
  $self->dbh->do($sql);

}

sub updateObjectInDB {
  my $self = shift;
  my $id = shift;
  my $objectRef = shift;
  my $table = shift;
  my %object = %$objectRef;
  my $updateStr = "";
  foreach my $k(keys %object) {
    next if substr($k,0,1) eq '#';
    if($object{$k}) {
      $updateStr .= $k.' = '. $self->dbh->quote($object{$k}).', ';
    }
  }
  chop $updateStr;
  chop $updateStr;
  my $sql = "update ".$table." set $updateStr where id = '$id'";
  $self->dbh->do($sql);
}

sub deleteObjectFromDB {
  my $self = shift;
  my $id = shift;
  my $table = shift;
  if($self->idExistsInDBTable($id, $table)) {
    my $sql = "delete from $table where id = $id";
    $self->dbh->do($sql);
  }
}

sub add_mm_author {
  my $self = shift;
  my %mm_author = @_;
  my $table = $self->{'mm_author_table'};
  foreach my $author_id (keys %mm_author) {
    my $sql = "delete from $table where author_id='$author_id'";
    $self->dbh->do($sql);
    foreach my $v (@{$mm_author{$author_id}}) {
      $sql = "insert into $table (author_id, publication_id) values ($author_id,".'$$'.$v.'$$)';
      $self->dbh->do($sql);
    }
  }
}

sub add_mm_favourites {
  my $self = shift;
  my %mm_author = @_;
  my $table = $self->{'mm_favourites_table'};
  foreach my $author_id (keys %mm_author) {
    my $sql = "delete from $table where author_id='$author_id'";
    $self->dbh->do($sql);
    foreach my $v (@{$mm_author{$author_id}}) {
      $sql = "insert into $table (author_id, publication_id) values ($author_id,".'$$'.$v.'$$)';
      $self->dbh->do($sql);
    }
  }
}

1;
