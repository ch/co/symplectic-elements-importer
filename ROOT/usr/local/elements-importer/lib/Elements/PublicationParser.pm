package Elements::PublicationParser;

use strict;
use warnings;

use XML::LibXML;
use XML::LibXML::XPathContext;
use Data::Dumper;
use lib '.';


my $recordsxpath = './api:records';

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {
    'xc' => XML::LibXML::XPathContext->new(),
    'parser' => XML::LibXML->new(),
    'xpathPublicationInfo' => { 'title' => 'api:field[@name="title"]/api:text',
      'journal' => 'api:field[@name="journal"]/api:text',
      'begin-page' => 'api:field[@name="pagination"]/api:pagination/api:begin-page',
      'end-page' => 'api:field[@name="pagination"]/api:pagination/api:end-page',
      'volume' => 'api:field[@name="volume"]/api:text',
      'doi' => 'api:field[@name="doi"]/api:text',
      'number' => 'api:field[@name="number"]/api:text',
      'author-url' => 'api:field[@name="author-url"]/api:text',
    },
    'publicationAttributes' => ['last-modified-when', 'type-id'],
  };
  $self->{'xc'}->registerNs( 'atom' => 'http://www.w3.org/2005/Atom' );
  bless($self, $class);
  return $self;
}

sub xc {
  my $self = shift;
  return $self->{'xc'};
}

sub parser {
  my $self = shift;
  return $self->{'parser'};
}

sub parsePublicationsFromString {
  my $self = shift;
  my $xml = shift;
  my $doc = $self->{'parser'}->parse_string($xml);
  $self->{'xc'}->setContextNode($doc->documentElement());
  return $self->parsePublications;
}

sub parsePublicationsFromFile {
  my $self = shift;
  my $filename = shift;
  my $doc = $self->{'parser'}->parse_file($filename);
  $self->{'xc'}->setContextNode($doc->documentElement());
  return $self->parsePublications;
}

sub parsePublications {
  my $self = shift;
  my %publications = ();
  foreach my $publicationNode ($self->{'xc'}->findnodes('/atom:feed/atom:entry/api:object[@category="publication"]')) {
    my $id = $publicationNode->getAttribute('id');
    my %pubInfo = $self->parseRecordSet($publicationNode);
    $self->parseYearMonthDay($publicationNode, \%pubInfo);
    foreach my $attr (@{$self->{'publicationAttributes'}}) {
      $pubInfo{$attr} = $publicationNode->getAttribute($attr);
    }
    $pubInfo{'reporting-date'} = $self->parseRecordDate($publicationNode);
    replaceHyphensInArrayKeys(\%pubInfo);
    $pubInfo{'authors'} = $self->parseAuthors($publicationNode);
    $pubInfo{'#duplicates'} = $self->findDuplicates($publicationNode);
    if(not defined ($pubInfo{'title'}) or $pubInfo{'title'} eq "") {
      handleMissingTitle(\%pubInfo);
    }
    $publications{$id}=\%pubInfo;
  }
  return %publications;
}

sub handleMissingTitle {
  my $info = shift;
# We ultimately require a non-empty title - so if we had no title in the recordset,
# try to make one up but fall back to a space if need be.
  my $title = ' ';
  if($info->{'journal'} ne '') {
    $title = $info->{'journal'};
    $title .= " " . $info->{'volume'} if $info->{'volume'} ne '';
    $title .= " " . $info->{'begin_page'} if $info->{'begin_page'} ne '';
  }
  $info->{'title'} = $title;
}

sub parseRecordDate {
  my $self = shift;
  my $publicationNode = shift;
  my $date = $publicationNode->findvalue('./api:reporting-date-1');
  return $date;
}

sub parseRecordSet {
  my $self = shift;
  my $publicationNode = shift;

  my $recordset = $publicationNode->findnodes($recordsxpath.'/api:record');
  my $numRecords = $recordset->size();
  my %result = ();
  for my $el (keys %{$self->{'xpathPublicationInfo'}}) {

    my $value;
    my %values = ();

    for(my $i=1; $i<=$numRecords; $i++)  {
      my $score=0;

      my $record_source = $publicationNode->findnodes($recordsxpath."/api:record[$i]")->get_node(0)->getAttribute('source-name');
      $value = $publicationNode->findnodes($recordsxpath."/api:record[$i]/api:native/".$self->{'xpathPublicationInfo'}->{$el});
      my $type_els = $publicationNode->findnodes($recordsxpath."/api:record[$i]/api:native/api:field[\@name='types']/api:items/api:item");
      my @types = ();
      foreach my $type ($type_els->get_nodelist) {
	      push(@types, $type->string_value);
      }


      # if we have an 'Early Access' record, we should prefer other records
      if(grep (/^Early Access$/, @types)) {
        $score -= 200;
      }

# actively encourage LaTeX, given that we handle this with mathjax on the websites
      if ( $value =~ /\$/ ) { $score += 100; }

# DOIs in 10.1101 are from the bioarxiv. We see cases where one record source has
# the bioarxiv DOI but another source has the updated DOI once the paper has been
# published. We thus downrank the bioarxiv DOI so other sources will be preferred
# if available.
      if ( $el eq 'doi' and $value =~ /^10\.1101\//) { $score -= 200; }

# rough intention is to lightly down-score records based on their source
# and then far more heavily down-score based on other attributes

# c-inst-1 are records imported from Apollo. Empirically, these can often
# be manually entered before final publication and thus are not necessarily
# the "final" record
      if ( $record_source eq 'c-inst-1' ) { $score -= 200; }
      if ( $record_source eq 'arxiv' ) { $score -= 200; }

      if ( $value =~ /^[A-Z \W]+$/ ) { $score -= 10; }
      if ( $value =~ /[\r\n]+/ ) { $score -= 10; }
# rank "empty" values into oblivion
      if ( $value =~ /^\s*$/ ) { $score -= 9999; }
      $values{$value} = $score;
    }
    $result{$el} = keyWithLargestValue(%values);
  }

  if ( defined $result{'doi'} && $result{'doi'} ne '' ) { delete $result{'author-url'}; }

  replaceHyphensInArrayKeys(\%result);

  return %result;
}

# we parse year, month, day seperately - even though they're seperate elements in the XML,
# they logically all constitute "the date"
sub parseYearMonthDay {
  my $self = shift;
  my $publicationNode = shift;
  my $pubInfo = shift;

  my $recordset = $publicationNode->findnodes($recordsxpath.'/api:record');
  my $numRecords = $recordset->size();
  my %result = ();
  my $value;
  my %values = ();
  for(my $i=1; $i<=$numRecords; $i++)  {
    my $record_source = $publicationNode->findnodes($recordsxpath."/api:record[$i]")->get_node(0)->getAttribute('source-name');

    my $year = $publicationNode->findnodes($recordsxpath."/api:record[$i]/api:native/api:field[\@name='publication-date']/api:date/api:year");
    my $month = $publicationNode->findnodes($recordsxpath."/api:record[$i]/api:native/api:field[\@name='publication-date']/api:date/api:month");
    my $day = $publicationNode->findnodes($recordsxpath."/api:record[$i]/api:native/api:field[\@name='publication-date']/api:date/api:day");

    my $score = 0;

# prefer as many of year/month/day as are available. But note
# we then downrank c-inst-1 and arxiv sources by enough that
# that they are never preferred if other sources are available

    if($year and $month and $day) {
      $score = 3;
    } elsif($year and $month) {
      $day = 1; # set default if we have no day
        $score = 2;
    } elsif($year) {
      $month = 1; # set default if we have no month..
        $day = 1;   # ..and no day
        $score = 1;
    } else {
      $score = -100;
    }

    $value=join("-", $year,$month,$day);

# c-inst-1 are records imported from Apollo. Empirically, these can often
# be manually entered before final publication and thus are not necessarily
# the "final" record
    if ( $record_source eq 'c-inst-1' ) { $score -= 10; }
    if ( $record_source eq 'arxiv' ) { $score -= 5; }
    $values{$value} = $score;
  }

  my($year,$month,$day) = split("-", keyWithLargestValue(%values));
  $pubInfo->{'publication-year'} = $year;
  $pubInfo->{'publication-month'} = $month;
  $pubInfo->{'publication-day'} = $day;

}

sub replaceHyphensInArrayKeys {
  my @keysToChange = ();
  foreach my $key (keys %{$_[0]}) {
    if ( $key =~ /\-/ ) { push(@keysToChange, $key); }
  }
  foreach my $key (@keysToChange) {
    my $val = ${$_[0]}{$key};
    delete(${$_[0]}{$key});
    my $newKey = $key;
    $newKey =~ tr/\-/_/;
    ${$_[0]}{$newKey}=$val;
  }
}

sub findDuplicates {
  my $self = shift;
  my $publicationNode = shift;
  my $duplicatexpath = './api:merge-history/api:merge/api:merged-object';
  my $duplicates = $publicationNode->findnodes($duplicatexpath);
  my @dups = ();
  if($duplicates->size() > 0) {
    foreach my $duplicate ($duplicates->get_nodelist) {
      push @dups, $duplicate->getAttribute('id');
    }
  }

  return \@dups;
}

sub parseAuthors {
  my $self = shift;
  my $publicationNode = shift;
  my $recordpath = './api:records/api:record[1]/api:native';
  my $people = $publicationNode->findnodes($recordpath.'/api:field[@name="authors"]/api:people/api:person');
  my $authorString = "";
  foreach my $person ($people->get_nodelist) {
    my $surname = $person->find('./api:last-name');
    my $initials = $person->find('./api:initials');
    $authorString .= "$initials $surname, ";
  }
  chop $authorString; # remove space
    chop $authorString; # remove comma
    return $authorString;
}

sub keyWithLargestValue {

  my %hash = @_;
  my($key, @keys) = keys %hash;
  my($big, @vals) = values %hash;

  for(0..$#keys) {
    my $cur = $vals[$_];
    if($cur > $big) {
      $big = $cur;
      $key = $keys[$_];
    }
  }
  $key
}


1;
