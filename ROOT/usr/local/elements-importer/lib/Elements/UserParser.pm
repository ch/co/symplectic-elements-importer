package Elements::UserParser;

use strict;
use warnings;

use XML::LibXML;
use XML::LibXML::XPathContext;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {
    'xc' => XML::LibXML::XPathContext->new(),
    'parser' => XML::LibXML->new(),
    'userAttributes' => ['username', 'last-modified-when']
  };
  $self->{'xc'}->registerNs( 'atom' => 'http://www.w3.org/2005/Atom' );

  bless($self, $class);
  return $self;
}

sub xc {
  my $self = shift;
  return $self->{'xc'};
}

sub parser {
  my $self = shift;
  return $self->{'parser'};
}

sub parseUsersFromString {
  my $self = shift;
  my $xml = shift;
  my $doc = $self->{'parser'}->parse_string($xml);
  $self->{'xc'}->setContextNode($doc->documentElement());
  return $self->parseUsers;
}


sub parseUsersFromFile {
  my $self = shift;
  my $filename = shift;
  my $doc = $self->{'parser'}->parse_filename($filename);
  $self->{'xc'}->setContextNode($doc->documentElement());
  return $self->parseUsers;
}

sub parseUsers {
  my $self = shift;
  my %users = ();
  foreach my $user ($self->{'xc'}->findnodes('/atom:feed/atom:entry/api:object[@category="user"]')) {
    my $id = $user->getAttribute('id');
    my %userInfo = ();
    foreach my $attr (@{$self->{'userAttributes'}}) {
      $userInfo{$attr} = $user->getAttribute($attr);
    }
    $userInfo{'username'} = lc($userInfo{'username'});
    replaceHyphensInArrayKeys(\%userInfo);
    $users{$id} = \%userInfo;
  }
  return %users;
}


sub replaceHyphensInArrayKeys {
  my @keysToChange = ();
  foreach my $key (keys %{$_[0]}) {
    if ( $key =~ /\-/ ) { push(@keysToChange, $key); }
  }
  foreach my $key (@keysToChange) {
    my $val = ${$_[0]}{$key};
    delete(${$_[0]}{$key});
    my $newKey = $key;
    $newKey =~ tr/\-/_/;
    ${$_[0]}{$newKey}=$val;
  }
} 

1;
