package Elements::UserRelationParser;

use strict;
use warnings;

use XML::LibXML;
use XML::LibXML::XPathContext;
use Data::Dumper;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {
    'xc' => XML::LibXML::XPathContext->new(),
    'parser' => XML::LibXML->new(),
    'userAttributes' => ['username', 'last-modified-when']
  };
  $self->{'xc'}->registerNs( 'atom' => 'http://www.w3.org/2005/Atom' );

  bless($self, $class);
  return $self;
}

sub xc {
  my $self = shift;
  return $self->{'xc'};
}

sub parser {
  my $self = shift;
  return $self->{'parser'};
}

sub parseUserRelationsFromString {
  my $self = shift;
  my $xml = shift;
  my $doc = $self->{'parser'}->parse_string($xml);
  $self->{'xc'}->setContextNode($doc->documentElement());
  return $self->parseUserRelations;
}


sub parseUserRelationsFromFile {
  my $self = shift;
  my $filename = shift;
  my $doc = $self->{'parser'}->parse_file($filename);
  $self->{'xc'}->setContextNode($doc->documentElement());
  return $self->parseUserRelations;
}

sub parseUserRelations {
  my $self = shift;
  my @authors = ();
  my @favourites = ();
#type-id of 8 means "is author of"
  foreach my $relation ($self->{'xc'}->findnodes('/atom:feed/atom:entry/api:relationship[@type-id=8]')) {
    my $id = $relation->getAttribute('id');

    my %relationInfo = ();
    my $is_favourite = $relation->findvalue('./api:is-favourite');
    my $is_visible = $relation->findvalue('./api:is-visible');
    my $xpath = "/atom:feed/atom:entry/api:relationship[\@id='$id']".'/api:related/api:object[@category="publication"]';
    my $publication = $self->{'xc'}->findnodes($xpath);
    my $publication_id = $publication->get_node(1)->getAttribute('id');
    if($is_visible eq "true") {
      push(@authors, $publication_id);
    }
    if($is_favourite eq "true") {
      push(@favourites, $publication_id);	
    }	
  }
  return (\@authors, \@favourites);
}


sub replaceHyphensInArrayKeys {
  my @keysToChange = ();
  foreach my $key (keys %{$_[0]}) {
    if ( $key =~ /\-/ ) { push(@keysToChange, $key); }
  }
  foreach my $key (@keysToChange) {
    my $val = ${$_[0]}{$key};
    delete(${$_[0]}{$key});
    my $newKey = $key;
    $newKey =~ tr/\-/_/;
    ${$_[0]}{$newKey}=$val;
  }
} 

1;
