#!/usr/bin/perl

use strict;
use warnings;

use FindBin::libs;

use Elements::PublicationParser;
use Elements::UserParser;
use Elements::UserRelationParser;
use Elements::Database;
use Elements::Crawler;
use Elements::Config;
use Elements::Constants qw ( %LOGLEVEL );
use Data::Dumper;

sub usage {
 print "$0 [publication_id]\n";
 exit;
}

my $id = shift or usage;

my $config = Elements::Config->new('/etc/elements-importer/config.pl');

my $apicreds = $config->apicreds;

my $publicationParser = Elements::PublicationParser->new();

my $api_base = 'https://elements.admin.cam.ac.uk:8092/elements-api/v5.5';

my $crawler=Elements::Crawler->new();
$crawler->apicreds($apicreds);
$crawler->baseUrl($api_base);

$crawler->resourceType('publications');
$crawler->addUrlOption('detail', 'full');
$crawler->addUrlOption('ids', $id);

$crawler->timeout(5); # seconds
$crawler->stopAfterPage(1);
$crawler->delay(2/60); # time unit is minutes here
$crawler->addUrlOption('per-page', 25);

my $response = $crawler->crawlOnePage;
my %publications = $publicationParser->parsePublicationsFromString($response);

print Dumper %publications;
