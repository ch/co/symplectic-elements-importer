--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3 (Ubuntu 15.3-1.pgdg20.04+1)
-- Dumped by pg_dump version 15.3 (Ubuntu 15.3-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: www; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA www;


ALTER SCHEMA www OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: fn_display_author_crsids(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_display_author_crsids(integer) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
BEGIN
	return array_to_string(array(select username from mm_publication_id_author_view where publication_id = $1), $$ $$);
END;
$_$;


ALTER FUNCTION public.fn_display_author_crsids(integer) OWNER TO postgres;

--
-- Name: fn_display_author_crsids(integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_display_author_crsids(integer, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
delimiter alias for $2;
BEGIN
	return array_to_string(array(select username from mm_publication_id_author_view where publication_id = $1), delimiter);
END;
$_$;


ALTER FUNCTION public.fn_display_author_crsids(integer, text) OWNER TO postgres;

--
-- Name: fn_display_favourite_of_crsids(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_display_favourite_of_crsids(integer) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
BEGIN
	return array_to_string(array(select username from mm_publication_id_favourite_of_view where publication_id = $1), $$ $$);
END;
$_$;


ALTER FUNCTION public.fn_display_favourite_of_crsids(integer) OWNER TO postgres;

--
-- Name: fn_display_favourite_of_crsids(integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_display_favourite_of_crsids(integer, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
delimiter alias for $2;
BEGIN
	return array_to_string(array(select username from mm_publication_id_favourite_of_view where publication_id = $1), delimiter);
END;
$_$;


ALTER FUNCTION public.fn_display_favourite_of_crsids(integer, text) OWNER TO postgres;

--
-- Name: fn_display_pagination(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_display_pagination(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
begin_page alias for $1;
number alias for $2;

BEGIN
if begin_page is not null then
return begin_page;
else
return number;
end if;

END;
$_$;


ALTER FUNCTION public.fn_display_pagination(text, text) OWNER TO postgres;

--
-- Name: fn_display_title(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_display_title(text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
input alias for $1;
titlelength int;
maxlength int := 100;

BEGIN
select into titlelength char_length(input);
if titlelength > maxlength then
return substring(input from 1 for maxlength-3) || $$...$$;
else
return input;
end if;

END;
$_$;


ALTER FUNCTION public.fn_display_title(text) OWNER TO postgres;

--
-- Name: fn_publication_date(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_publication_date(integer, integer, integer) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
year  alias for $1;
month alias for $2;
day  alias for $3;
stringdate text;
BEGIN
-- give publications without a year an arbitrarily old date; this field is only used for sorting and filtering, never display
if year is not null then 
	select into stringdate year || $$-$$;
else 
	select into stringdate $$1900-$$;
end if;
if month is not null then
	select into stringdate stringdate || month || $$-$$;
else
	select into stringdate stringdate || $$1-$$;  
end if;
if day is not null then
	select into stringdate stringdate || day;
else
	select into stringdate stringdate || $$1$$;  
end if;
return stringdate;
END;
$_$;


ALTER FUNCTION public.fn_publication_date(integer, integer, integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: journal_names; Type: TABLE; Schema: public; Owner: elements_user
--

CREATE TABLE public.journal_names (
    id integer NOT NULL,
    name text NOT NULL,
    is_alias_of integer NOT NULL
);


ALTER TABLE public.journal_names OWNER TO elements_user;

--
-- Name: journal_names_id_seq; Type: SEQUENCE; Schema: public; Owner: elements_user
--

CREATE SEQUENCE public.journal_names_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.journal_names_id_seq OWNER TO elements_user;

--
-- Name: journal_names_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: elements_user
--

ALTER SEQUENCE public.journal_names_id_seq OWNED BY public.journal_names.id;


--
-- Name: mm_user_publications_author; Type: TABLE; Schema: public; Owner: elements_user
--

CREATE TABLE public.mm_user_publications_author (
    author_id integer,
    publication_id integer
);


ALTER TABLE public.mm_user_publications_author OWNER TO elements_user;

--
-- Name: publications; Type: TABLE; Schema: public; Owner: elements_user
--

CREATE TABLE public.publications (
    id integer NOT NULL,
    title character varying NOT NULL,
    last_modified_when timestamp without time zone,
    journal text,
    volume text,
    begin_page text,
    end_page text,
    number text,
    authors text,
    publication_day integer,
    publication_month integer,
    publication_year integer,
    doi text,
    type_id integer,
    reporting_date date,
    reporting_date_override date,
    manually_hide_from_feed boolean,
    author_url text
);


ALTER TABLE public.publications OWNER TO elements_user;

--
-- Name: users; Type: TABLE; Schema: public; Owner: elements_user
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying NOT NULL,
    last_modified_when timestamp without time zone
);


ALTER TABLE public.users OWNER TO elements_user;

--
-- Name: mm_publication_id_author_view; Type: VIEW; Schema: public; Owner: elements_user
--

CREATE VIEW public.mm_publication_id_author_view AS
 SELECT users.username,
    mm_user_publications_author.publication_id
   FROM ((public.publications
     JOIN public.mm_user_publications_author ON ((publications.id = mm_user_publications_author.publication_id)))
     JOIN public.users ON ((users.id = mm_user_publications_author.author_id)));


ALTER TABLE public.mm_publication_id_author_view OWNER TO elements_user;

--
-- Name: mm_user_publications_favourite; Type: TABLE; Schema: public; Owner: elements_user
--

CREATE TABLE public.mm_user_publications_favourite (
    author_id integer,
    publication_id integer
);


ALTER TABLE public.mm_user_publications_favourite OWNER TO elements_user;

--
-- Name: mm_publication_id_favourite_of_view; Type: VIEW; Schema: public; Owner: elements_user
--

CREATE VIEW public.mm_publication_id_favourite_of_view AS
 SELECT users.username,
    mm_user_publications_favourite.publication_id
   FROM ((public.publications
     JOIN public.mm_user_publications_favourite ON ((publications.id = mm_user_publications_favourite.publication_id)))
     JOIN public.users ON ((users.id = mm_user_publications_favourite.author_id)));


ALTER TABLE public.mm_publication_id_favourite_of_view OWNER TO elements_user;

--
-- Name: publications_view; Type: VIEW; Schema: public; Owner: elements_user
--

CREATE VIEW public.publications_view AS
 SELECT publications.id,
    public.fn_display_title((publications.title)::text) AS display_title,
    publications.title,
    publications.journal,
    public.fn_display_pagination(publications.begin_page, publications.number) AS pagination,
    publications.volume,
    publications.authors,
    publications.doi,
    public.fn_display_author_crsids(publications.id, ','::text) AS author_crsids,
    public.fn_display_favourite_of_crsids(publications.id, ','::text) AS favourite_crsids,
    publications.publication_day,
    publications.publication_month,
    publications.publication_year,
    publications.last_modified_when,
    publications.reporting_date AS publication_date
   FROM public.publications;


ALTER TABLE public.publications_view OWNER TO elements_user;

--
-- Name: publications_view_day; Type: VIEW; Schema: public; Owner: elements_user
--

CREATE VIEW public.publications_view_day AS
 SELECT publications_view.id,
    publications_view.display_title,
    publications_view.title,
    publications_view.journal,
    publications_view.pagination,
    publications_view.volume,
    publications_view.authors,
    publications_view.doi,
    publications_view.author_crsids,
    publications_view.favourite_crsids,
    publications_view.publication_day,
    publications_view.publication_month,
    publications_view.publication_year,
    publications_view.last_modified_when,
    publications_view.publication_date
   FROM public.publications_view
  WHERE (publications_view.last_modified_when > (('now'::text)::date - '3 days'::interval))
UNION
 SELECT publications_view.id,
    publications_view.display_title,
    publications_view.title,
    publications_view.journal,
    publications_view.pagination,
    publications_view.volume,
    publications_view.authors,
    publications_view.doi,
    publications_view.author_crsids,
    publications_view.favourite_crsids,
    publications_view.publication_day,
    publications_view.publication_month,
    publications_view.publication_year,
    publications_view.last_modified_when,
    publications_view.publication_date
   FROM (public.publications_view
     LEFT JOIN public.mm_user_publications_author ON ((mm_user_publications_author.publication_id = publications_view.id)))
  WHERE (mm_user_publications_author.publication_id IS NULL);


ALTER TABLE public.publications_view_day OWNER TO elements_user;

--
-- Name: test_pubs; Type: TABLE; Schema: public; Owner: elements_user
--

CREATE TABLE public.test_pubs (
    id integer NOT NULL,
    title character varying NOT NULL,
    last_modified_when timestamp without time zone,
    journal text,
    volume text,
    begin_page text,
    end_page text,
    number text,
    authors text,
    publication_day integer,
    publication_month integer,
    publication_year integer,
    doi text,
    type_id integer,
    reporting_date date,
    reporting_date_override date,
    manually_hide_from_feed boolean
);


ALTER TABLE public.test_pubs OWNER TO elements_user;

--
-- Name: www_mm_publication_author; Type: VIEW; Schema: public; Owner: elements_user
--

CREATE VIEW public.www_mm_publication_author AS
 SELECT mm_user_publications_author.publication_id,
    (array_to_string(array_agg(users.username), ','::text) || ','::text) AS author_crsids
   FROM ((public.publications
     JOIN public.mm_user_publications_author ON ((publications.id = mm_user_publications_author.publication_id)))
     JOIN public.users ON ((users.id = mm_user_publications_author.author_id)))
  GROUP BY mm_user_publications_author.publication_id;


ALTER TABLE public.www_mm_publication_author OWNER TO elements_user;

--
-- Name: www_mm_publication_fav; Type: VIEW; Schema: public; Owner: elements_user
--

CREATE VIEW public.www_mm_publication_fav AS
 SELECT mm_user_publications_favourite.publication_id,
    (array_to_string(array_agg(users.username), ','::text) || ','::text) AS favourite_crsids
   FROM ((public.publications
     JOIN public.mm_user_publications_favourite ON ((publications.id = mm_user_publications_favourite.publication_id)))
     JOIN public.users ON ((users.id = mm_user_publications_favourite.author_id)))
  GROUP BY mm_user_publications_favourite.publication_id;


ALTER TABLE public.www_mm_publication_fav OWNER TO elements_user;

--
-- Name: www_publications; Type: VIEW; Schema: public; Owner: elements_user
--

CREATE VIEW public.www_publications AS
 SELECT publications.id,
    public.fn_display_title((publications.title)::text) AS display_title,
    publications.title,
    publications.journal,
    public.fn_display_pagination(publications.begin_page, publications.number) AS pagination,
    publications.volume,
    publications.authors,
    publications.doi,
    publications.author_url,
    m1.author_crsids,
    m2.favourite_crsids,
    publications.publication_day,
    publications.publication_month,
    COALESCE((date_part('year'::text, publications.reporting_date))::integer, publications.publication_year) AS publication_year,
    publications.last_modified_when,
        CASE
            WHEN (publications.reporting_date IS NOT NULL) THEN publications.reporting_date
            WHEN ((publications.publication_year IS NOT NULL) AND (publications.publication_month IS NOT NULL) AND (publications.publication_day IS NOT NULL)) THEN (((((publications.publication_year || '-'::text) || publications.publication_month) || '-'::text) || publications.publication_day))::date
            ELSE NULL::date
        END AS publication_date
   FROM ((public.publications
     LEFT JOIN public.www_mm_publication_author m1 ON ((publications.id = m1.publication_id)))
     LEFT JOIN public.www_mm_publication_fav m2 ON ((publications.id = m2.publication_id)))
  WHERE ((publications.type_id <> 29) AND (publications.manually_hide_from_feed IS NOT TRUE) AND ((publications.title)::text !~~ '%Erratum%'::text))
  ORDER BY
        CASE
            WHEN (publications.reporting_date IS NOT NULL) THEN publications.reporting_date
            WHEN ((publications.publication_year IS NOT NULL) AND (publications.publication_month IS NOT NULL) AND (publications.publication_day IS NOT NULL)) THEN (((((publications.publication_year || '-'::text) || publications.publication_month) || '-'::text) || publications.publication_day))::date
            ELSE NULL::date
        END DESC NULLS LAST;


ALTER TABLE public.www_publications OWNER TO elements_user;

--
-- Name: mm_publication_author; Type: VIEW; Schema: www; Owner: elements_user
--

CREATE VIEW www.mm_publication_author AS
 SELECT mm_user_publications_author.publication_id,
    (array_to_string(array_agg(users.username), ','::text) || ','::text) AS author_crsids
   FROM ((public.publications
     JOIN public.mm_user_publications_author ON ((publications.id = mm_user_publications_author.publication_id)))
     JOIN public.users ON ((users.id = mm_user_publications_author.author_id)))
  GROUP BY mm_user_publications_author.publication_id;


ALTER TABLE www.mm_publication_author OWNER TO elements_user;

--
-- Name: mm_publication_fav; Type: VIEW; Schema: www; Owner: elements_user
--

CREATE VIEW www.mm_publication_fav AS
 SELECT mm_user_publications_favourite.publication_id,
    (array_to_string(array_agg(users.username), ','::text) || ','::text) AS favourite_crsids
   FROM ((public.publications
     JOIN public.mm_user_publications_favourite ON ((publications.id = mm_user_publications_favourite.publication_id)))
     JOIN public.users ON ((users.id = mm_user_publications_favourite.author_id)))
  GROUP BY mm_user_publications_favourite.publication_id;


ALTER TABLE www.mm_publication_fav OWNER TO elements_user;

--
-- Name: publications; Type: VIEW; Schema: www; Owner: elements_user
--

CREATE VIEW www.publications AS
 SELECT publications.id,
    public.fn_display_title((publications.title)::text) AS display_title,
    publications.title,
    publications.journal,
    public.fn_display_pagination(publications.begin_page, publications.number) AS pagination,
    publications.volume,
    publications.authors,
    publications.doi,
    m1.author_crsids,
    m2.favourite_crsids,
    publications.publication_day,
    publications.publication_month,
    publications.publication_year,
    publications.last_modified_when,
    publications.reporting_date AS publication_date
   FROM ((public.publications
     LEFT JOIN www.mm_publication_author m1 ON ((publications.id = m1.publication_id)))
     LEFT JOIN www.mm_publication_fav m2 ON ((publications.id = m2.publication_id)))
  ORDER BY publications.reporting_date DESC NULLS LAST;


ALTER TABLE www.publications OWNER TO elements_user;

--
-- Name: journal_names id; Type: DEFAULT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.journal_names ALTER COLUMN id SET DEFAULT nextval('public.journal_names_id_seq'::regclass);


--
-- Name: journal_names is_alias_of; Type: DEFAULT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.journal_names ALTER COLUMN is_alias_of SET DEFAULT currval('public.journal_names_id_seq'::regclass);


--
-- Name: journal_names journal_names_pkey; Type: CONSTRAINT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.journal_names
    ADD CONSTRAINT journal_names_pkey PRIMARY KEY (id);


--
-- Name: publications publications_pkey; Type: CONSTRAINT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.publications
    ADD CONSTRAINT publications_pkey PRIMARY KEY (id);


--
-- Name: test_pubs test_pubs_pkey; Type: CONSTRAINT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.test_pubs
    ADD CONSTRAINT test_pubs_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: journal_names journal_names_is_alias_of_fkey; Type: FK CONSTRAINT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.journal_names
    ADD CONSTRAINT journal_names_is_alias_of_fkey FOREIGN KEY (is_alias_of) REFERENCES public.journal_names(id);


--
-- Name: mm_user_publications_author mm_user_publications_author_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.mm_user_publications_author
    ADD CONSTRAINT mm_user_publications_author_author_id_fkey FOREIGN KEY (author_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: mm_user_publications_author mm_user_publications_author_publication_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.mm_user_publications_author
    ADD CONSTRAINT mm_user_publications_author_publication_id_fkey FOREIGN KEY (publication_id) REFERENCES public.publications(id) ON DELETE CASCADE;


--
-- Name: mm_user_publications_favourite mm_user_publications_favourite_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.mm_user_publications_favourite
    ADD CONSTRAINT mm_user_publications_favourite_author_id_fkey FOREIGN KEY (author_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: mm_user_publications_favourite mm_user_publications_favourite_publication_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: elements_user
--

ALTER TABLE ONLY public.mm_user_publications_favourite
    ADD CONSTRAINT mm_user_publications_favourite_publication_id_fkey FOREIGN KEY (publication_id) REFERENCES public.publications(id) ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: TABLE journal_names; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.journal_names TO elements_user_ro;


--
-- Name: TABLE mm_user_publications_author; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.mm_user_publications_author TO elements_user_ro;


--
-- Name: TABLE publications; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.publications TO elements_user_ro;


--
-- Name: TABLE users; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.users TO elements_user_ro;


--
-- Name: TABLE mm_publication_id_author_view; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.mm_publication_id_author_view TO elements_user_ro;


--
-- Name: TABLE mm_user_publications_favourite; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.mm_user_publications_favourite TO elements_user_ro;


--
-- Name: TABLE mm_publication_id_favourite_of_view; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.mm_publication_id_favourite_of_view TO elements_user_ro;


--
-- Name: TABLE publications_view; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.publications_view TO elements_user_ro;


--
-- Name: TABLE publications_view_day; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.publications_view_day TO elements_user_ro;


--
-- Name: TABLE test_pubs; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.test_pubs TO elements_user_ro;


--
-- Name: TABLE www_mm_publication_author; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.www_mm_publication_author TO elements_user_ro;


--
-- Name: TABLE www_mm_publication_fav; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.www_mm_publication_fav TO elements_user_ro;


--
-- Name: TABLE www_publications; Type: ACL; Schema: public; Owner: elements_user
--

GRANT SELECT ON TABLE public.www_publications TO elements_user_ro;


--
-- PostgreSQL database dump complete
--

