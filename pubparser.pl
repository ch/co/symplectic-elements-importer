#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

use Elements::PublicationParser;
use Elements::UserParser;
use Elements::UserRelationParser;
use Elements::Database;
use Elements::Crawler;
use DBI;

my $dbUser = 'symplectic_user';
my $dbPass = 'T6Afr9We';
my $dbHost = '127.0.0.1';
my $dbName = 'symplectics';
my $publicationsTable = 'publications';

my $db = Elements::Database->new();
$db->dbUser($dbUser);
$db->dbPass($dbPass);
$db->dbHost($dbHost);
$db->dbName($dbName);
$db->publicationsTable($publicationsTable);
$db->connectToDB;

my $filename = $ARGV[0];

my $pubParser = Elements::PublicationParser->new();

my %publications = $pubParser->parsePublicationsFromFile($filename);

for my $id (keys %publications) {
	$db->addPublication($id, $publications{$id});
	if(@{$publications{$id}->{'#duplicates'}}) {
		foreach my $duplicate (@{$publications{$id}->{'#duplicates'}}) {
			$db->deletePublication($duplicate);
		}
	}
}

