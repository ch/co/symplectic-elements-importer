#!/usr/bin/perl

use strict;
use warnings;
use lib '.';

use Data::Dumper;

use Elements::UserRelationParser;

my $filename = $ARGV[0];

my $relationParser = Elements::UserRelationParser->new();

my ($authors, $favourites) = $relationParser->parseUserRelationsFromFile($filename);

my %auth = ();

push(@{$auth{'1178'}}, @{$authors});
push(@{$auth{'1234'}}, @{$authors});
foreach my $k (keys %auth) {
	foreach my $v (@{$auth{$k}}) {
		print "$k: $v\n";
	}
}
